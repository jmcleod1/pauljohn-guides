#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans helvet
\font_typewriter courier
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 0
\use_mhchem 0
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Count model notes
\end_layout

\begin_layout Author
Paul Johnson <pauljohn@ku.edu>
\end_layout

\begin_layout Date
2011
\end_layout

\begin_layout Section
Overview
\end_layout

\begin_layout Standard
For more reading on Count models, consult the following, probably in this
 order:
\end_layout

\begin_layout Standard
Scott Long, Regression Models for Categorical and Limited Dependent Variables,
 Chapter 8, 
\begin_inset Quotes eld
\end_inset

Count outcomes
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
Gary King.
 1988.
 Statistical Models for Political Science Event Counts: Bias in Conventional
 Procedures and Evidence for the Exponential Poisson Regression Model, 
\emph on
American Journal of Political Science
\emph default
, 32(3): 838-863.
\end_layout

\begin_layout Standard
Gary King.
 1989.
 Variance Specification in Event Count Models: From Restrictive Assumptions
 to a Generalized Estimator.
 
\emph on
American Journal of Political Science
\emph default
 33(3): 762-784
\end_layout

\begin_layout Standard
Cameron and Trivedi, 1998.
 
\emph on
Regression Analysis of Count Data
\emph default
, Cambridge University Press.
 
\end_layout

\begin_layout Subsection
Motivation
\end_layout

\begin_layout Standard
The dependent variable is a count, which means it has values from 0 on up.
 Clearly, OLS does not fit.
\end_layout

\begin_layout Standard
Well, if the expected number of observed cases is large, and the distribution
 of the count data is drawn from the Poisson distribution, then OLS might
 be OK.
 The Poisson is not all that different from a Normal distribution.
 So sometimes the Normal case can be thought of as an approximation.
\end_layout

\begin_layout Standard
But if the expected number of counts is small, the Poisson distribution
 is not even a little bit like a normal distribution.
\end_layout

\begin_layout Subsection
Alternatives might not be as good.
\end_layout

\begin_layout Standard
Think of things like
\end_layout

\begin_layout Itemize
tobit
\end_layout

\begin_layout Itemize
ordinal logit/probit
\end_layout

\begin_layout Subsection
Generalized Linear Model.
 
\end_layout

\begin_layout Standard
Think back to Ordinary Least Squares.
 We asserted the dependent variable 
\begin_inset Formula $y_{i}$
\end_inset

 as a sum of a 
\begin_inset Quotes eld
\end_inset

predictable part
\begin_inset Quotes erd
\end_inset

 and a 
\begin_inset Quotes eld
\end_inset

random
\begin_inset Quotes erd
\end_inset

 part: 
\begin_inset Formula $y_{i}=b_{0}+b_{1}x_{i}+e_{i}$
\end_inset

.
\end_layout

\begin_layout Standard
If we were to assert that 
\begin_inset Formula $e_{i}$
\end_inset

 is 
\begin_inset Quotes eld
\end_inset

Normal with a mean of 0 and standard deviation of 
\begin_inset Formula $\sigma_{e}$
\end_inset


\begin_inset Quotes erd
\end_inset

, than would imply that 
\begin_inset Formula $y_{i}$
\end_inset

 is 
\begin_inset Quotes eld
\end_inset

Normal with a mean of 
\begin_inset Formula $b_{0}+b_{1}x_{i}$
\end_inset

 and a standard deviation of 
\begin_inset Formula $\sigma_{e}$
\end_inset

.
 So OLS with an assumed Normal error implies
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
y_{i}\sim N(X_{i}b,\,\,\sigma_{e}^{2})
\]

\end_inset


\end_layout

\begin_layout Standard
The symbol 
\begin_inset Quotes eld
\end_inset


\begin_inset Formula $\sim$
\end_inset


\begin_inset Quotes erd
\end_inset

 means 
\begin_inset Quotes eld
\end_inset

is distributed as
\begin_inset Quotes erd
\end_inset

 or 
\begin_inset Quotes eld
\end_inset

is drawn from
\begin_inset Quotes erd
\end_inset

.
 
\begin_inset Formula $X_{i}b$
\end_inset

 is shorthand matrix terminology for the 
\begin_inset Quotes eld
\end_inset

linear predictor
\begin_inset Quotes erd
\end_inset

, 
\begin_inset Formula $b_{o}+b_{1}x1_{i}+b_{2}x2_{i}$
\end_inset

.
 
\end_layout

\begin_layout Standard
What's the big point here?
\end_layout

\begin_layout Quote
We think of the predictor as determining a parameter in a distribution from
 which observations are drawn.
 
\end_layout

\begin_layout Subsection
You can use any distribution you want
\end_layout

\begin_layout Standard
You can model 
\begin_inset Formula $y_{i}$
\end_inset

 by any distribution you want, and you can let the properties of that distributi
on depend on input variables and parameters.
 For a 
\begin_inset Quotes eld
\end_inset

count
\begin_inset Quotes erd
\end_inset

 model, all you absolutely need is the requirement that 
\begin_inset Formula $y_{i}\geq0$
\end_inset

.
 It just so happens that people in the past have worked out these models
 with Poisson and Negative Binomial, so you can follow them.
 But you don't have to.
\end_layout

\begin_layout Section
Poisson.
\end_layout

\begin_layout Standard
The Poisson is a 
\begin_inset Quotes eld
\end_inset

one parameter
\begin_inset Quotes erd
\end_inset

 distribution.
 Usually it is called 
\begin_inset Formula $\lambda$
\end_inset

, and that parameter determines the expected value and the variance.
\end_layout

\begin_layout Subsection
formula (blah!)
\end_layout

\begin_layout Standard
Instead of the greek letter 
\begin_inset Formula $\lambda$
\end_inset

, let's call it what we mean: 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Pr(y_{i}|input_{i})=\frac{exp(-input_{i})input_{i}^{y_{i}}}{y_{i}!}
\]

\end_inset

For any 
\begin_inset Formula $y_{i}$
\end_inset

 you put in here, this tells you how likely you are to count that many 
\begin_inset Quotes eld
\end_inset

things
\begin_inset Quotes erd
\end_inset

 if the input is 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

.
 When I write 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

, I hope you the combined impact of parameters and variables.
 
\end_layout

\begin_layout Standard
Input is not necessarily simply 
\begin_inset Formula $X_{i}b$
\end_inset

.
 In fact, we usually have to 
\begin_inset Quotes eld
\end_inset

translate
\begin_inset Quotes erd
\end_inset

 or 
\begin_inset Quotes eld
\end_inset

curve
\begin_inset Quotes erd
\end_inset

 the linear predictor so it fits 
\begin_inset Quotes eld
\end_inset

within boundaries.
\begin_inset Quotes erd
\end_inset

 
\end_layout

\begin_layout Standard
So 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

 is typically some function that depends on 
\begin_inset Formula $X_{i}b$
\end_inset

, for generality, 
\begin_inset Formula $g(X_{i}b)$
\end_inset

.
\end_layout

\begin_layout Subsection
Graph the Poisson Distribution to Get a 
\begin_inset Quotes eld
\end_inset

Feeling
\begin_inset Quotes erd
\end_inset

.
\end_layout

\begin_layout Standard
Consider Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "cap:Poisson-Distribution,-small"

\end_inset

 and 
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Poisson Distribution, small expected value (
\begin_inset Formula $\lambda$
\end_inset

)
\begin_inset CommandInset label
LatexCommand label
name "cap:Poisson-Distribution,-small"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/poissonPlots1.eps

\end_inset


\end_layout

\end_inset

Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "cap:Poisson-Distribution,-larger"

\end_inset

 which present histograms for sample sizes of 500 that were drawn from various
 Poisson distributions.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Poisson Distribution, larger expected value (
\begin_inset Formula $\lambda$
\end_inset

)
\begin_inset CommandInset label
LatexCommand label
name "cap:Poisson-Distribution,-larger"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/poissonPlots2.eps

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Noteworthy: 
\end_layout

\begin_layout Enumerate
The Expected Value of the Poisson is 
\begin_inset Formula $\lambda="input"$
\end_inset

.
\end_layout

\begin_layout Enumerate
The Variance of the Poisson is 
\begin_inset Formula $\lambda="input"$
\end_inset

.
\end_layout

\begin_layout Enumerate
The shape changes and gets 
\begin_inset Quotes eld
\end_inset

more normal
\begin_inset Quotes erd
\end_inset

 as 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

 gets bigger.
\end_layout

\begin_deeper
\begin_layout Standard
Implication: If your count data has high values, then the OLS Normal model
 may serve about as well as a Poisson model
\end_layout

\begin_layout Standard
However, there are 2 problems.
\end_layout

\begin_layout Enumerate
Nonlinearity.
\end_layout

\begin_layout Enumerate
Heteroskedasticity.
\end_layout

\end_deeper
\begin_layout Subsection
Nonlinear Transformation of 
\begin_inset Formula $X_{i}b$
\end_inset

 Required for Poisson
\end_layout

\begin_layout Standard

\series bold
The 
\begin_inset Formula $input_{i}$
\end_inset

 must be positive!
\series default
 We are considering a 
\begin_inset Quotes eld
\end_inset

count variable,
\begin_inset Quotes erd
\end_inset

 something that is always POSITIVE.
 The expected value of a Poisson variable has to be positive.
 Since the expected value equals the value of 
\begin_inset Quotes eld
\end_inset


\begin_inset Formula $input_{i}$
\end_inset


\begin_inset Quotes erd
\end_inset

, then 
\begin_inset Formula $X_{i}b$
\end_inset

 cannot serve as 
\begin_inset Formula $input_{i}$
\end_inset

 because it may be negative.
 
\end_layout

\begin_layout Standard
All kinds of transformations have been considered to make sure input is
 positive.
 A common way is to say that the input should be exponentiated, because
 exp(anything) is positive.
 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
input_{i}=exp(X_{i}b)
\]

\end_inset


\end_layout

\begin_layout Standard
Now, that results in the stupid looking exp(exp) appearance of the Poisson
 regression model:
\begin_inset Formula 
\[
Pr(y|Xb)=\frac{exp(-exp(Xb))(exp(Xb))^{y}}{y!}
\]

\end_inset


\end_layout

\begin_layout Standard
or it looks slightly less ugly (not much) if we write:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Pr(y|Xb)=\frac{exp(-e^{Xb})(e^{Xb})^{y}}{y!}
\]

\end_inset


\end_layout

\begin_layout Subsection
King called it the 
\begin_inset Quotes eld
\end_inset

Exponential Poisson
\begin_inset Quotes erd
\end_inset

 model, others call it the 
\begin_inset Quotes eld
\end_inset

log link
\begin_inset Quotes erd
\end_inset

.
\end_layout

\begin_layout Standard
If
\begin_inset Formula 
\[
input_{i}=exp(X_{i}b)
\]

\end_inset


\end_layout

\begin_layout Standard
then
\begin_inset Formula 
\[
log(input_{i})=X_{i}b
\]

\end_inset


\end_layout

\begin_layout Standard
In the Generalized Linear Model literature, they think of the transformation
 happing to the left hand side, they call it the link function.
 
\end_layout

\begin_layout Standard
So the exponential on the right is the 
\begin_inset Quotes eld
\end_inset

inverse link
\begin_inset Quotes erd
\end_inset

 function.
\end_layout

\begin_layout Subsection
Estimation: straight forward ML
\end_layout

\begin_layout Standard
Adjust the b's to maximize the product of the probabilities of the observations:
\begin_inset Formula 
\[
L(b;y,X)=Pr(y_{1}|Xb)*Pr(y_{2}|Xb)*...*Pr(y_{N}|Xb)
\]

\end_inset


\end_layout

\begin_layout Standard
Usually, one would take logs, and maximize the log likelihood, which would
 be a sum.
\end_layout

\begin_layout Subsection
Interpretation
\end_layout

\begin_layout Standard
Recall the expected value of 
\begin_inset Formula $y_{i}$
\end_inset

 given input is just the input itself.
 So that means if the estimate is 
\begin_inset Formula $\hat{b}$
\end_inset

 
\begin_inset Formula 
\[
E(y_{i}|X_{i})=exp(X_{i}\hat{b})
\]

\end_inset


\end_layout

\begin_layout Standard
So if the k'th variable changes, the impact is
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{\partial E(y_{i}|X_{i})}{\partial x_{k}}=\hat{b}_{k}*E(y_{i}|X_{i})
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
=\hat{b}_{k}*exp(X_{i}\hat{b})
\]

\end_inset


\end_layout

\begin_layout Standard
Long discusses the calculation of the percent change in expected y, i.e.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{E(y_{i}|X_{i},x_{k}+\delta)}{E(y_{i}|X_{i},x_{k})}=exp(\hat{b}*\delta)
\]

\end_inset


\end_layout

\begin_layout Section
Negative Binomial
\end_layout

\begin_layout Subsection
Poisson Weaknesses
\end_layout

\begin_layout Enumerate
Poisson is a 
\begin_inset Quotes eld
\end_inset

one parameter
\begin_inset Quotes erd
\end_inset

 model.
 The variance is not separately under our control.
 Maybe we could find a two parameter distribution with a more well-suited
 variance parameter.
\end_layout

\begin_layout Enumerate
Repeat the same point: The Poisson may not fit the data because the variance
 predicted by the Poisson may be too small for the observed data.
\end_layout

\begin_layout Subsection
Negative Binomial Motivation: Dispersion
\end_layout

\begin_layout Standard
As Long says p.230, the Negative Binomial can be described in a number of
 ways.
 
\end_layout

\begin_layout Standard
I think the 
\begin_inset Quotes eld
\end_inset

extra randomness
\begin_inset Quotes erd
\end_inset

 interpretation is the simplest.
 
\end_layout

\begin_layout Standard
Suppose 
\begin_inset Formula $input_{i}$
\end_inset

 has an additional amount of randomness.
 This new random error that causes 
\begin_inset Quotes eld
\end_inset

heterogeneity
\begin_inset Quotes erd
\end_inset

 (sometimes the term 
\begin_inset Quotes eld
\end_inset

frailty
\begin_inset Quotes erd
\end_inset

 is used) in the outputs for cases that have the same observed values of
 
\begin_inset Formula $X_{i}$
\end_inset

.
 
\end_layout

\begin_layout Standard
Suppose the Poisson process has an expected value of the original thing
 I called 
\begin_inset Quotes eld
\end_inset

input
\begin_inset Quotes erd
\end_inset

 times another random variable:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
new\, input_{i}=input_{i}*\delta_{i}
\]

\end_inset


\end_layout

\begin_layout Standard
Note that if
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\delta_{i}=1
\]

\end_inset


\begin_inset Newline newline
\end_inset

then this thing just degenerates back to the original Poisson model.
 
\end_layout

\begin_layout Standard
In the most common version of the Poisson model, we use the 
\begin_inset Quotes eld
\end_inset

log link
\begin_inset Quotes erd
\end_inset

, which means that the input is exponentiated.
 So the original Poisson model input
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
input_{i}=exp(X_{i}b)
\]

\end_inset


\begin_inset Newline newline
\end_inset

can be supplemented by an additional error term that is added in the parentheses
:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
new\, input_{i}=exp(X_{i}b+u_{i})
\]

\end_inset


\begin_inset Newline newline
\end_inset

and it is easy enough to see that is equal to
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
new\, input_{i}=exp(X_{i}b)\times exp(u_{i})
\]

\end_inset


\begin_inset Newline newline
\end_inset

So one can either think of the new error as an additive bit of noise with
 the linear predictor (
\begin_inset Formula $+u_{i})$
\end_inset

 or a multiplicative effect applied to the transformed linear predictor
 (
\begin_inset Formula $\times\delta_{i}=exp(u_{i})$
\end_inset

).
 
\end_layout

\begin_layout Standard
Of course, we can convert 
\begin_inset Quotes eld
\end_inset

back and forth
\begin_inset Quotes erd
\end_inset

 between the multiplicative error 
\begin_inset Formula $\delta_{i}$
\end_inset

 and the additive error 
\begin_inset Formula $u_{i}$
\end_inset

, thus: 
\begin_inset Formula 
\[
u_{i}=log(\delta_{i})
\]

\end_inset


\end_layout

\begin_layout Subsubsection*
Vital to Pick 
\begin_inset Formula $\delta_{i}$
\end_inset

 Distribution Properly
\end_layout

\begin_layout Standard
It is necessary to assume that this additive noise is 
\begin_inset Quotes eld
\end_inset

neutral
\begin_inset Quotes erd
\end_inset

, in the sense that it causes more uncertainty, but it does not change the
 average outcome.
 That is true if
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
E[\delta_{i}]=1\Longrightarrow E(exp(u_{i}))=1
\]

\end_inset


\begin_inset Newline newline
\end_inset

or, equivalently, 
\begin_inset Formula 
\[
E[u_{i}]=0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

On average
\begin_inset Quotes erd
\end_inset

 the extra error term has 
\begin_inset Quotes eld
\end_inset

no effect
\begin_inset Quotes erd
\end_inset

.
\end_layout

\begin_layout Subsection*
Output is Conditional Poisson Model
\end_layout

\begin_layout Standard
The maximum likelihood estimation has to be amended to incorporate a new
 likelihood component for each case.
 Hence, our theory says that GIVEN 
\begin_inset Formula $X_{i}$
\end_inset

 and an additional perturbation 
\begin_inset Formula $u_{i}$
\end_inset

, the probability model is a a Poisson process.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
P(y_{i}|X_{i},u_{i})=\frac{exp(-new\, input_{i})\times new\, input_{i}^{y_{i}}}{y!}.
\]

\end_inset


\end_layout

\begin_layout Standard
The input on the right side includes the additional frailty.
\end_layout

\begin_layout Subsection
Gamma distributed heterogeneity
\end_layout

\begin_layout Standard
The most commonly assumed probability distribution for 
\begin_inset Formula $\delta_{i}=exp(u_{i})$
\end_inset

 is the Gamma distribution in which the dispersion is controlled by a parameter
 
\begin_inset Formula $v$
\end_inset

.
 The full Gamma distribution has two parameters, but we are going to arrange
 them so we only need to worry about one, which determines the variance.
 This simplification of the gamma can be done in several ways, which will
 be outlined later.
 
\end_layout

\begin_layout Standard
The key think is this: If 
\begin_inset Formula $\delta_{i}$
\end_inset

 is drawn from 
\begin_inset Quotes eld
\end_inset

a properly selected
\begin_inset Quotes erd
\end_inset

 gamma distribution, then 
\begin_inset Formula $E(\delta_{i})=1$
\end_inset

 and 
\begin_inset Formula $Var[\delta_{i}]=1/some\, parameter\, we\, choose$
\end_inset

.
\end_layout

\begin_layout Subsection
Gamma distribution background
\end_layout

\begin_layout Standard
The Gamma describes the probability of a continuous variable on 
\begin_inset Formula $[0,\infty].$
\end_inset

 It can look like a 
\begin_inset Quotes eld
\end_inset

ski slope
\begin_inset Quotes erd
\end_inset

 or it can look single-peaked.
 See Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Gamma-Distribution"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/GammaPDF.pdf
	width 3in

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Gamma Distribution
\begin_inset CommandInset label
LatexCommand label
name "fig:Gamma-Distribution"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
It has 2 parameters, 
\begin_inset Formula $shape$
\end_inset

 and 
\begin_inset Formula $scale$
\end_inset

.
 In some books, the 
\begin_inset Formula $scale$
\end_inset

 parameter is replaced by a parameter called 
\begin_inset Formula $rate$
\end_inset

, which is equal to 
\begin_inset Formula $1/scale$
\end_inset

.
\end_layout

\begin_layout Standard
If 
\begin_inset Formula $\delta_{i}$
\end_inset

 is Gamma distributed, the probability density function is:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f(\delta_{i})=\frac{1}{scale^{shape}\Gamma(shape)}\delta_{i}^{(shape-1)}e^{-(\frac{\delta_{i}}{scale})}
\]

\end_inset


\end_layout

\begin_layout Subsection
What is that Gamma function?
\end_layout

\begin_layout Standard
The function 
\begin_inset Formula $\Gamma(shape)$
\end_inset

 is the Gamma function (which is a complicated math thing I've never looked
 into very much).
 It is 
\begin_inset Formula $\Gamma(s)=\int_{0}^{\infty}t^{s-1}e^{-t}dt$
\end_inset

 if 
\begin_inset Formula $s>0$
\end_inset

.
 If you pick s as an integer, 
\begin_inset Formula $\Gamma(s)$
\end_inset

 is very easy to calculate:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\Gamma(s)=(s-1)!\,\,\, s=1,2,...
\]

\end_inset


\end_layout

\begin_layout Standard
So, the value of 
\begin_inset Formula $\Gamma(1)=1.$
\end_inset

 And 
\begin_inset Formula $\Gamma(2)=1.$
\end_inset

 And 
\begin_inset Formula $\Gamma(20)$
\end_inset

 is some impossibly huge number.
 I never really worry about it, but in case you do, here's a graph of it:
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/GammaFunct.eps
	width 3in

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
The Gamma Function
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Adjust the Gamma PDF to create the right kind of heterogeneity.
\end_layout

\begin_layout Standard
The two parameter Gamma probability distribution has these interesting propertie
s:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
E(\delta_{i})=shape*scale
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Var(\delta_{i})=shape*scale^{2}
\]

\end_inset


\end_layout

\begin_layout Subsubsection
Simple, Direct approach: 
\begin_inset Formula $scale=1/shape$
\end_inset


\end_layout

\begin_layout Standard
We want to simplify this formula so that it depends on only one parameter.
 The most direct simplification is to set 
\begin_inset Formula $scale=1/shape$
\end_inset

.
 
\end_layout

\begin_layout Standard
So, the expected value and variance are 
\begin_inset Formula 
\[
E[\delta_{i}]=shape/shape=1
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Var[\delta_{i}]=shape/shape^{2}=1/shape
\]

\end_inset


\end_layout

\begin_layout Standard
So the 
\begin_inset Formula $shape$
\end_inset

 parameter is 
\begin_inset Quotes eld
\end_inset


\begin_inset Formula $some\, parameter\, we\, choose"$
\end_inset

.
 In this formulation, it is easy to see that if 
\begin_inset Formula $shape$
\end_inset

 is very large, then the variance of 
\begin_inset Formula $\delta_{i}$
\end_inset

 is very small.
 The extra heterogeneity has only a minor effect, and, in fact, as 
\begin_inset Formula $shape$
\end_inset

 tends to 
\begin_inset Formula $\infty$
\end_inset

, the value of 
\begin_inset Formula $\delta_{i}$
\end_inset

 collapses around 
\begin_inset Formula $1.0$
\end_inset

.
\end_layout

\begin_layout Subsubsection
Another approach that ends up at the same place
\end_layout

\begin_layout Standard
I have seen some presentations that simplify the Gamma in a different way.
 First, fix scale = 1.
 Let's call this new intermediate random variable 
\begin_inset Formula $m_{i}$
\end_inset

, because it will not have just the right properties.
 Then the probability density formula with scale=1 simplifies to:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f(m_{i})=\frac{1}{\Gamma(shape)}m_{i}^{(shape-1)}e^{-m{}_{i}}\,\,\,\, shape>0
\]

\end_inset


\end_layout

\begin_layout Standard
If shape=1, then this is an exponential distribution (because 
\begin_inset Formula $\Gamma(1)=1).$
\end_inset

 
\end_layout

\begin_layout Standard
The expected value and variance are:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
E[m_{i}]=shape
\]

\end_inset

and
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Var[m_{i}]=shape.
\]

\end_inset


\begin_inset Newline newline
\end_inset

The advantage of this formulation is that we can easily see what we need
 to do to convert 
\begin_inset Formula $m_{i}$
\end_inset

 into our final result:
\begin_inset Formula 
\[
\delta_{i}=\frac{m_{i}}{shape}
\]

\end_inset


\end_layout

\begin_layout Standard
Notice that after diving each draw by 
\begin_inset Formula $shape,$
\end_inset

 we have a variable with just the same properties as the other formulation.
\end_layout

\begin_layout Standard
\begin_inset Formula $E(\delta_{i})=E(\frac{m_{i}}{shape})=\frac{1}{shape}E(m_{i})=\frac{shape}{shape}=1$
\end_inset


\end_layout

\begin_layout Standard
and also
\end_layout

\begin_layout Standard
\begin_inset Formula $V(\delta_{i})=V(\frac{m_{i}}{shape})=\frac{1}{shape^{2}}V(m_{i})=\frac{shape}{shape^{2}}=\frac{1}{shape}$
\end_inset


\end_layout

\begin_layout Standard
If you go back and forth between books, you get a headache because no two
 books seem to write this down in exactly the same way.
 But I'm pretty sure I've written it down correctly.
\end_layout

\begin_layout Standard
I wondered what a variable such 
\begin_inset Formula $m_{i}/shape$
\end_inset

 would look like, so I made a figure.
 See Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "gamma"

\end_inset

.
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Formula $m_{i}/shape\,$
\end_inset

where 
\begin_inset Formula $m\sim Gamma(1,shape)$
\end_inset


\begin_inset CommandInset label
LatexCommand label
name "gamma"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/GammaDivShape1.eps
	width 6in

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
And, if you are like me, and you wonder what the histogram of 
\begin_inset Formula $log(\frac{v_{i}}{shape})$
\end_inset

would look like, I'm proud to say I have that too in Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "logvdivshape"

\end_inset

.
 Please note that, as the math above indicates, the center of the distribution
 of 
\begin_inset Formula $log(\frac{v_{i}}{shape})$
\end_inset

 is centered on 0, and that as the shape increases, the variance of the
 distribution shrinks dramatically.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Formula $log(v_{i}/shape)$
\end_inset

 where 
\begin_inset Formula $v\sim Gamma(1,shape)$
\end_inset


\begin_inset CommandInset label
LatexCommand label
name "logvdivshape"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/logGammaDivShape1.eps
	width 6in

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Estimating 
\end_layout

\begin_layout Standard
I believe, if you wanted to, you could use a general purpose program for
 the estimation of 
\begin_inset Quotes eld
\end_inset

generalized linear mixed models
\begin_inset Quotes erd
\end_inset

 to estimate the b as well as the shape parameter.
 In doing so, you'd treat 
\begin_inset Formula $b$
\end_inset

 as a 
\begin_inset Quotes eld
\end_inset

fixed
\begin_inset Quotes erd
\end_inset

 effect and then there is the additive random parameter that has the Gamma
 distribution.
 You'd want to estimate the shape parameter in order to find out how much
 uncertainty there is in the model.
 
\end_layout

\begin_layout Standard
However, there is a more elegant approach that works for this special case.
 
\end_layout

\begin_layout Standard
Here's the way we are going to think of this.
 There is a two-stage process.
 The shape estimate is chosen first.
 Then, knowing that, a draw from the Poisson distribution with mean 
\begin_inset Formula $input_{i}*\delta_{i}$
\end_inset

 is taken.
 Since, for each observation, a different 
\begin_inset Formula $\delta_{i}$
\end_inset

 is drawn, and then that is put together with the input before the 
\begin_inset Formula $y_{i}$
\end_inset

 is drawn, it is possible for 2 cases with the exact same input 
\begin_inset Formula $e^{X_{i}b}$
\end_inset

could have 
\begin_inset Formula $y_{i}$
\end_inset

's drawn from different Poisson distributions.
\end_layout

\begin_layout Standard
Tools from probability theory will lead to an exact probability model for
 this.
 
\end_layout

\begin_layout Standard
If
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Y\,|\,\delta_{i}\sim Poisson(input_{i}*\delta_{i})
\]

\end_inset


\end_layout

\begin_layout Standard
then
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{y}(y|shape,input)=\frac{\Gamma(shape+y)}{\Gamma(shape)y!}\bullet\frac{input^{y}shape^{shape}}{(input+shape)^{shape+y}}
\]

\end_inset


\end_layout

\begin_layout Standard
(Venables and Ripley, 4th ed, p.
 206)
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
E(y_{i})=input
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Var(y_{i})=input+input^{2}/shape
\]

\end_inset


\end_layout

\begin_layout Standard
Note that if 
\begin_inset Formula $shape=\infty$
\end_inset

, then the variance of 
\begin_inset Formula $y_{i}$
\end_inset

 is just 
\begin_inset Formula $input_{i}$
\end_inset

, meaning the original Poisson model is back! But for other values of the
 shape parameter, the variance of 
\begin_inset Formula $y_{i}$
\end_inset

 is greater than in the Poisson model.
\end_layout

\begin_layout Subsection
About that 
\begin_inset Quotes eld
\end_inset

shape
\begin_inset Quotes erd
\end_inset

 parameter
\end_layout

\begin_layout Standard
Fitting is an iterative process.
 The 
\begin_inset Quotes eld
\end_inset

shape
\begin_inset Quotes erd
\end_inset

 parameter has to be iterated after the model is fit, and the model has
 to be fit in light of the estimate of the shaper parameter.
 The MASS package for R provides a procedure 
\begin_inset Quotes eld
\end_inset

glm.nb
\begin_inset Quotes erd
\end_inset

 which will do maximum likelihood to estimate the b's and the shape parameter.
 (In Venables & Ripley, p.
 207, the 
\begin_inset Quotes eld
\end_inset

shape
\begin_inset Quotes erd
\end_inset

 parameter is called 
\begin_inset Formula $\theta$
\end_inset

.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Gamma probability distribution
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename importfigs/GammaDistrib.eps
	width 7in
	height 9in

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
NB estimation
\end_layout

\begin_layout Standard
The results indicate one surprise, that the expected value of y is the same
 in the Poisson and the NB model.
 However, the variance is different.
 In the NB model, the variance is
\begin_inset Formula 
\[
Var(y_{i}|X)=exp(X_{i}b)\left(1+\frac{exp(X_{i}b)}{v_{i}}\right)
\]

\end_inset


\end_layout

\begin_layout Subsection
Overdispersion
\end_layout

\begin_layout Standard
Estimates from a Poisson model are inefficient and have bad standard errors
 if the data is really produced by a heterogeneous process of the NB sort.
\end_layout

\begin_layout Standard
Note the Poisson model is really 
\begin_inset Quotes eld
\end_inset

nested
\begin_inset Quotes erd
\end_inset

 inside the NB model.
 If we do a significance test of 
\begin_inset Formula $H_{o}:\alpha=0$
\end_inset

 and cannot reject it, then it means we ought to go back to the Poisson.
 Long p.
 237 discusses other tests.
 See the R package pscl for a test that can be used.
\end_layout

\begin_layout Section
Zero inflated models
\end_layout

\begin_layout Standard
The Poisson or NB models might not match the data because they don't have
 enough observed 0's.
\end_layout

\begin_layout Standard
The 
\begin_inset Quotes eld
\end_inset

fix
\begin_inset Quotes erd
\end_inset

 is to think of the probability process as a two step thing.
 First, the observed y is either 0 or a number 
\begin_inset Formula $y_{i}.$
\end_inset

 Whether it is observed or not is modeled by any dichotomous regression
 model, such as logit or probit.
 Second, if it is observed, the count is given by one of the models above.
\end_layout

\begin_layout Standard
All kinds of details flow forth if you get into writing out one of these
 models.
 Should the predictors in the dichotomous regression exercise be the same
 ones that are used in the Poisson or NB regression? Should we insist the
 predictive part of the probit model is proportional to the count model?
\end_layout

\begin_layout Standard
Now, how can a probability process give back a 0?
\end_layout

\begin_layout Standard
Either through the failure of the probit stage or a predicted 0 from the
 count stage, so in the Poisson 
\begin_inset Formula 
\[
P(y_{i}=0|X_{i})=\psi_{i}+(1-\psi_{i})*exp(-exp(Xb))
\]

\end_inset


\end_layout

\begin_layout Standard
(Write out the poisson for y=0 to understand the last term).
\end_layout

\begin_layout Standard
And the probability of any other value is given by the regular poisson,
 multiplied by 
\begin_inset Formula $(1-\psi_{i}):$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
P(y_{i}|X_{i})=(1-\psi_{i})*Poisson(Xb)
\]

\end_inset

 
\end_layout

\end_body
\end_document
