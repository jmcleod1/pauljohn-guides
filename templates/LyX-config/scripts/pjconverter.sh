#!/bin/bash

rm -rf $4.pdf 
rm -rf "$2/plots"
rm -rf "$2/*.pdf"

R CMD Sweave $1 
rsync -av $1 $3;

if [ -d $2/plots* ]; then 
rsync -rav $2/plots* $3; 
fi


pjrtangle.sh "$1"
rsync -av *.R $3

sleep 3 

exit 0
 
