#!/bin/bash


FROMFILE=$1
TOFILE=$2

##First, copy the pdf output that was already made back to document directory
echo "Now try cp $FROMFILE $TOFILE"
cp $FROMFILE $TOFILE


FILEBASE=`basename $2`
FILENOEXT=${FILEBASE%.*}


WORKDIR=`dirname $FROMFILE`

echo "pjBeamerMultiOutCopier: here is argument 2: $2"
echo "pjBeamerMultiOutCopier: here is FILEBASE: $FILEBASE"
echo "pjBeamerMultiOutCopier: here is FILENOEXT: $FILENOEXT"

DOCDIR=`dirname $TOFILE`
cd $DOCDIR

cp "$FILENOEXT.lyx" "$FILENOEXT-article.lyx" 

perl -pi.bak -e 's/sweavel-beamer/sweavel-beamer-article/' "$FILENOEXT-article.lyx"

## FIXME 20130502
## This was obliterating the plots folder
## lyx -e pdf6 "$FILENOEXT-article.lyx"


sleep 1 

exit 0
 
