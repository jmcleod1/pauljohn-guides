## Paul Johnson
## Adapted from lyx literate-scrap.inc.

Format 2

Preamble
\usepackage{Sweavel}
EndPreamble

OutputType              literate

Style Scrap
  LatexType             paragraph
  LatexName             dummy
  Margin                static
  TopSep                0.4
  BottomSep             0.4
  ParSep                0.4
  LabelType             static
  Align                 left
  AlignPossible         block, left, right, center
  NewLine               0
  PassThru              1
  FreeSpacing           1
  Spacing               single
  TextFont
    Color               latex
    Family              typewriter
  EndFont
End

Style Sinput
  LatexType             Environment
  LatexName             Sinput
  Margin                static
  TopSep                0.4
  BottomSep             0.4
  ParSep                0.4
  LabelType             static
  Align                 left
  AlignPossible         block, left, right, center
  NewLine               0
  PassThru              1
  FreeSpacing           1
  TextFont
    Color               latex
    Family              typewriter
  EndFont
End


Style Soutput
  LatexType             Environment
  LatexName             Soutput
  Margin                static
  TopSep                0.4
  BottomSep             0.4
  ParSep                0.4
  LabelType             static
  Align                 left
  AlignPossible         block, left, right, center
  NewLine               0
  PassThru              1
  FreeSpacing           1
  TextFont
    Color               latex
    Family              typewriter
  EndFont
End

Style Scode
  LatexType             Environment
  LatexName             Scode
  Margin                static
  TopSep                0.4
  BottomSep             0.4
  ParSep                0.4
  LabelType             static
  Align                 left
  # We need all of them, due to figures - works from LyX 1.4.4
  AlignPossible         block, left, right, center
  NewLine               0
  PassThru              1
  FreeSpacing           1
  TextFont
    Color               latex
    Family              typewriter
  EndFont
End
