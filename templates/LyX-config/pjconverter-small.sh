#!/bin/bash

## ONLY creates the Rnw file.
## does not do the copying of residual files back
## Arguments. $1 is Rnw name
## $2 working directory
## $3 input directory
## $4 input file without extension

rm -rf $4.pdf 
#rm -rf "$2/plots"
rm -rf "$2/*.pdf"

R CMD Sweave $1 


sleep 1 

exit 0
 
