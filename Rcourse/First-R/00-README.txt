The First-R tutorial series has been moved to
a repository owned by the CRMDA at KU. If you want access, 
I can arrange it.
