/*Now we are going to create Student objects and put them
into an array.  The first thing I will do is create an array of
Strings where I'll keep names.  Then I'll create student objects.


Note I'm now using the specific import for the Random class, as
I use this at the end to make it interesting.
*/


import java.util.Random;


public class Test{

    public static void main (String s[]) {

	int mySize = 10;
	String[]  myStringArray ;
	myStringArray = new String[] { "omur","frank","katie","d","e","f","g","h","i","j" };
	Student[]  myStudentArray;
		
 	myStudentArray = new Student[mySize];

	//Now create students and give them names out of myStringArray

	for (int i=0; i< mySize; i++){
	    myStudentArray[i] = new Student(myStringArray[i]);
	}


	//I'm scared to death this is nonsense, so lets tell the
	//student with offset 4 (that's the fifth one) to sayHiTo
	//the one at offset 1 (that's the second one)

	myStudentArray[4].sayHiTo( myStudentArray[1]);


	//see what happens if you say hi to yourself
	myStudentArray[4].sayHiTo( myStudentArray[4]);
	

	//Let's use the random number generator to cause
	//some random interactions between students.

	//First, lets be brave and create a new object from
	//which to ask for random numbers.  Look for the Random
	//class in the jdk documents.  We create the generator:

	Random myGenerator = new Random();
	

	//Then ask for a random number
	System.out.println("The next random number is " + myGenerator.nextInt());


	//Then ask for a random number between 0 and 10, not including 10

	System.out.println("The next random number in {0,...,9} is " + myGenerator.nextInt(10));


	//Now lets over and over tell these artificial students to greet one another

	for (int i = 0; i < 100; i++) {
	    Student x = myStudentArray[myGenerator.nextInt(10)];
	    Student y = myStudentArray[myGenerator.nextInt(10)];
		
	    x.sayHiTo(y);
	}
    }

}
