//This shows how to create a separate class in a separate file.
//Student is a class that doesn't do much, but the program creates
//two student objects, and then the first one says hi to the other.
//Note the Student class has the object's name set in there when
//it is created.

//The output you see on the screen should be this:

/*
I katie, was greeted by frank
The message was "hello from frank"
*/
//Those print statements are in the Student class, where the
//object knows its own name and it is able to ask the other student
//for its name.

//Note in the Student class I show an example of a method that returns
//a value (getName returns a String) and 

//I don't think these import is necessary because the program compiles
//and runs without them. But it just looked funny
//to me to have a program with no imports.

import java.lang.Object;
import java.lang.String;

public class Test extends Object{


    public static void main (String[] args) {
	Student frank = new Student("frank");
	Student katie = new Student("katie");
    
	frank.sayHiTo(katie);
	   
    }
}
