//This example is just here to show 2 things.

//Create a subclass from BigCalculator.  Call it JumboCalculator.
//Notice how new methods can be added, but we can still call methods defined
//in BigCalculator.

//Also, notice in JumboCalculator I've included the java Math class and there
//is "reportSomeCalculations()" method that uses some math tools from Java. 
//Note that these math tools are all used as "static methods" of the Math 
//class, so if we want the sine of some number, we do Math.sin(number).

//In the output you should notice a rounding "error" or numerical
//imprecision. The value of sin(pi) is an example 


import java.lang.Object;
import java.lang.String;

public class Test extends Object{


    public static void main (String[] args) {
	double d;

	JumboCalculator myCalculator = new JumboCalculator();
	d = myCalculator.multiplyAndAdd(8,4.343);
	System.out.println("multiplyAndAdd(8,4.343)= " + d);

	d = myCalculator.divideAndAdd(32, 2.12);
	System.out.println("divideAndAdd(32,2.12) = " + d);

	System.out.println("Calculator's magic number is " + myCalculator.getMagicNumber());

	myCalculator.reportSomeCalculations();
	
    }
}
