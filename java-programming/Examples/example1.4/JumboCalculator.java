public class JumboCalculator extends BigCalculator {

    //here is an instance variable.
    double magicNumber;

    public JumboCalculator(){ 
	//this constructor does nothing!
    }


    public void reportSomeCalculations(){

	System.out.println("The square root of 20 is: " + Math.sqrt(20));

	System.out.println("The value of 3.2 to the 2.4th power is: " + Math.pow(3.2,2.4));
	System.out.println("The value of the natural log of 5 is: " + Math.log( 5.0 ) );

	System.out.println("The value of sin(1) is: " + Math.sin( 1.0 ) );

	System.out.println("The value of PI is: " + Math.PI);

	System.out.println("The value of sin(pi) is: " + Math.sin( Math.PI ) );

	System.out.println("The value of sin(3.1415927) is: " + Math.sin(3.1415927 ) );

	System.out.println("The value of sin(0) is: " + Math.sin( 0.0 ) );

    }
}
