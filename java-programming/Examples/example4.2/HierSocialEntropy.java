import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.TreeSet;
import java.lang.Math;



public class  HierSocialEntropy {
  
  public static double  calculateHSE(int num, double[][] diss, double maximumDistance){
    int i,j;
    double entropyForK;
    int nThreshCoord = 0;
    
    ArrayList clusters;
    double[]  thresholdArray;
    double HSE;

   
    //  #ifdef debug
    //   System.out.println("distances are ");
    //      for (i = 0; i < num; i++)  {
   //  	for (j = 0; j < num; j++){
    //  	    System.out.println (" diss[" + i + "][" + j +"]= " + diss[i][j]);
    //  	}
    //  	System.out.println(" ");
    //      }
    //         System.out.println(" ");
    //  #endif

    int nOfThresholds= 1+(num * (num-1))/2;

    thresholdArray = new  double[nOfThresholds];

    thresholdArray[0] = 0.0;

    for (i = 0; i < num; i++)
      {
	for (j = i+1; j < num; j++)
	  {
	    nThreshCoord++;
	    thresholdArray [nThreshCoord] = diss [i][j];
	  }
      }

    //  #ifdef DEBUG
    //           System.out.println("Unsorted threshold values are:" );
    //                  for (i = 0; i< nThreshCoord+1; i++)
    //        	    System.out.print(" "  +  thresholdArray[i]);
    //        	  System.out.println(" ");
    //        	  System.out.println("nThreshCoord= " + nThreshCoord);

    //  #endif
     
    Arrays.sort(thresholdArray);

    //  #ifdef DEBUG
    //        System.out.println("Num is " + num + " and num of threshold coordinates is " +  nThreshCoord + "plus 1");
    //        	  System.out.println("sorted thresholds are:" );
    //        	  for (i = 0; i< nThreshCoord+1; i++)
    //        	    System.out.print(" " +  thresholdArray[i]);
    //        	  System.out.println("");
    //  #endif

    UDiamCluster aClusterTool =  new UDiamCluster(num,diss);
    HSE=0;
    
    for (j=0; j < nThreshCoord; j++) {
	double width = thresholdArray[j+1] - thresholdArray[j]; 
	clusters = aClusterTool.clusterK ( thresholdArray[j] ); 
	
	entropyForK = calculateEntropyForClusters(clusters);
	HSE += (entropyForK * width / maximumDistance);
	//  #ifdef DEBUG
	//  System.out.println("HSE got back " + clusters.size() + "from tool");
	//         aClusterTool.printMembers();
       	//       System.out.println("K: " + thresholdArray[j] + "entropyForK: " + entropyForK+ "HSE =  " + HSE);
	//  #endif
      }
  

    // #ifdef DEBUG
    // If you worry about not calculating entropy for the largest threshold, where
    // all objects are in the same cluster and E=0, go ahead and uncomment this:
    //   clusters = aClusterTool.clusterK ( thresholdArray[nThreshCoord] );
    // entropyForK = calculateEntropyForClusters(clusters);
    //   HSE += (entropyForK * (maximumDistance - thresholdArray[nThreshCoord])/maximumDistance);
    // System.out.println("K: " + thresholdArray[nThreshCoord] + "entropyForK: " + entropyForK+ "HSE =  " + HSE);


    return HSE;
  }



  public static  double  calculateEntropyForClusters (List list){
    int i = 0;
    int j = 0;
    int numClusters = 0;
    int totalMemberships = 0;

    double entropy = 0;
    TreeSet aCluster;

    numClusters = list.size();

    int []  sizes= new int[numClusters];
  
    for (Iterator index = list.iterator(); index.hasNext(); )
      {
	aCluster = (TreeSet)index.next();
	sizes[i] = aCluster.size();
	totalMemberships += sizes[i];
	i++;
      }

    for ( j = 0; j < numClusters; j++) 
      {
	double  p;
        
	p = (double)sizes[j] / (double)totalMemberships;
	entropy += p *(-1.0)* Math.log(p)/Math.log(2);
      }
  
    return entropy;
  }
}














