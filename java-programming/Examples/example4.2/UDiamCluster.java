
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.HashMap;
import java.util.TreeSet;


public class UDiamCluster {
  int N;
  Integer[]  items;
  double[][] dissMatrix;
  ArrayList clusterList;


  public UDiamCluster(int num, double[][] diss){

    N = num;

    dissMatrix = diss;
  
    clusterList = new ArrayList(N);
    items = new Integer[N];
    for(int i = 0; i < N; i++)
      {
	TreeSet aCluster = null;
	items[i] = new Integer(i) ;
	aCluster = new TreeSet() ;
	aCluster.add(items[i]);     
	clusterList.add(aCluster);
      }
  }
    

  public ArrayList clusterK (double threshold){
    agglomerateClusters$Threshold(clusterList, threshold);
    elimRedundantClusters(clusterList);
    return clusterList;
  }

  public void agglomerateClusters$Threshold(ArrayList list, double k){
    int toofar = 0;

    for (int  i = 0; i < N; i ++){
      int ithItem = items[i].intValue();
      for (ListIterator index = list.listIterator(); index.hasNext(); ) {
	TreeSet aCluster = (TreeSet)index.next();
	toofar=0;
	for(Iterator clusterIndex = aCluster.iterator();clusterIndex.hasNext();) {
	  int nextItem = ((Integer)clusterIndex.next()).intValue();
	  if ( dissMatrix[ithItem][nextItem] > k ){
	    toofar = 1;
	    break;
	  }
	}    
	if (toofar == 0) aCluster.add(items[i]);  
	//note: This is a treeset, no need check if you are adding object
	//to a collection twice.
      }
    }
	

    //  #ifdef DEBUG
    // System.out.println("At end of agglomeration, I have " + list.size() + "clusters");
    //    index= [list begin: [self getZone] ];
    //    for (aCluster= [index next]; [index getLoc] ==Member; aCluster=[index next])
    //      { 
    //        int count = [aCluster getCount];
    //        fprintf(stderr,"After agglom,mem is %d \n", count);
    //      }
    //    [index drop];
    //  #endif
  }

  public void elimRedundantClusters(ArrayList list) {
    
    int numclusters = list.size();
	
    for (ListIterator index = list.listIterator(); index.hasNext(); ){
 
      Object cluster1 = index.next();
      
      int sublistStart = index.nextIndex();
      int sublistEnd = list.size();
      //System.out.println("List size, sublistStart, sublistEnd " + list.size() + " " +  sublistStart  + " " + sublistEnd);
	
      List secondList =list.subList(sublistStart,sublistEnd); 
	
      for (int j = 0; j < secondList.size();j++ ) {
	TreeSet cluster2 = (TreeSet) secondList.get(j);
	
	if (cluster2.equals(cluster1))
	  index.remove();
	break;
      }
    }
    // System.out.println("eliminated clusters, " + list.size() + " left");
  }

  public void printMembers(){
  
    TreeSet aCluster;
	
    for ( Iterator index = clusterList.listIterator()  ; index.hasNext(); ){ 
      aCluster = (TreeSet) index.next();
      int count = aCluster.size();
      System.out.println ("Cluster Membership.  There are " +  clusterList.size() + " Clusters" + "and in this cluster there are " + count + " objects" );
      int k = 0;
      for(Iterator index2= aCluster.iterator();index2.hasNext(); ){
	Integer anInteger = (Integer) index2.next();
	int intval = anInteger.intValue();
	System.out.println("Member " + k + " is " + intval);
	k++;
      }
    }    

  }

}











