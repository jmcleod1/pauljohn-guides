import java.util.Random;
import java.lang.Math;
import java.awt.Color;
import java.io.*;


public class HSETest {
   
   

    public static void main (String []args) {
   
	
	doHSE();

    }

    public static  void doHSE(){

	SwarmRaster   aRaster;
	aRaster= new SwarmRaster("PJ's Space",100,100,3);

	for(int i=0; i< 5; i++) {

	    
	    int N=6+5*i;
	    double[][][] positionMatrix = new double[N][2][2];
	    double [][] distMatrix = new double[N][N];

	    aRaster.clear();

	    if(i==3) aRaster.setPixelBuffer(Color.green);

	    generateData(N,positionMatrix);
	    
	    plotData(N,positionMatrix,aRaster);
	    
	    pause("Hit return to see HSE value");

	    calculateDistances(N,distMatrix,positionMatrix);
	    
	    
	    //     for (int i = 0; i < N; i++){
	    //        for (int j = 0; j < N; j++){
	    //  	System.out.print ( distMatrix[i][j] + " " );
	    //        }
	    //        System.out.println( "");
	    //      }
	    
	    
	    {
		double ent;
		ent = HierSocialEntropy.calculateHSE(N,distMatrix,Math.sqrt(20000)) ;
		System.out.println("\n \n HS Entropy is " + ent);
		//saveData(i,ent);
	    }
	    
	    System.out.println("Now I'm going to make you wait 5 seconds, just to prove I can make you wait for 5 seconds :>");
	    try {
		Thread.sleep(5000) ;// waits for 5 seconds : sleep(long milliseconds)
		// or Thread.sleep(long milliseconds, int nanoseconds)
	    }
	    catch (InterruptedException e) {
		// the pause has been interrupted
	    }
	    System.out.println("That didn't seem like 5 seconds, did it?");
	    
	
    }
    
    
    }


    public static void plotData(int n, double[][][] matrix,SwarmRaster raster){

	
	raster.clear();
	for (int i = 0; i < n; i++) {
	    // System.out.println("my x is " + matrix[i][0][1] + " y is " + matrix[i][1][0]);
	    raster.drawPointX$Y$Color ((int)matrix[i][0][1],(int)matrix[i][1][0],Color.red.getRGB());
	    raster.drawSelf();
      }
    }

    //waits for user to hit "enter"
    public static void pause(String prompt){  
	System.out.print(prompt + " ");
	    System.out.flush();
	    try
		{
		    int lastChar = System.in.read();
		    // System.out.println("now" + lastChar);  // abo debug
		}
	    catch(java.io.IOException e)
		{
		}
	    //flush();
	}


    public static void generateData(int n,double[][][] matrix){ 
	Random randGenerator = new Random();	

	for (int i = 0; i < n; i++) {
      
	    matrix[i][0][1] = 50 + 10*randGenerator.nextGaussian();
	    matrix[i][1][0] = 50 + 10*randGenerator.nextGaussian();
	}
    }

    public static void calculateDistances(int n,double[][] distmat,double[][][]posmat){

    for (int i = 0; i < n; i++){
	for (int j = i+1; j < n; j++){
	    double dist = Math.sqrt((posmat[i][0][1]-posmat[j][0][1])*
			(posmat[i][0][1]-posmat[j][0][1])*
			+  (posmat[i][1][0]-posmat[j][1][0])*
			(posmat[i][1][0]-posmat[j][1][0]));
	    distmat[i][j] = dist;
	    distmat[j][i] = dist;
	    
	}
	
    }
    }

    public static void saveData(int i, double x){
	try{
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("demo" + i + ".txt")));
	    out.println(i + " " + x);
	    out.close();

	}catch (IOException e) {
	    System.err.println ("end of stream");
	}
	

	//  	BufferedReader stdin = new BufferedReader (new InputStreamReader(System.in));
	//  	String str = new String();
//  	try {
//  	   str = stdin.readLine();
//   	}catch (IOException e) {
//             System.out.println("where's your input???");
//          }
    }


}
