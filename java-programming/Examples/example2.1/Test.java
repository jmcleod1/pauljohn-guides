//It is vital to understand arrays.  So this file just creates
//some arrays.  


/*
It is a key to understand that creating an array typically involves
two steps. I realize some shortcuts work in some case, but I don't use them, usually. Instead, I have 2 steps.

1. declare the array

For an array of integers do
    int[]  myIntArray;
or
    double[]  myDoubleArray;

2. create the array object with a "new" command. This has to
say precisely how many members the array has, here there are 14

    myIntArray = new int[14]

    myDoubleArray = new double[14];

Now, other things to note.  

1. An array has an ivar called "length" you can get to find the size
of an array.

2. Note the printTheArray methods below (polymorphism).  It is vital
you understand why I have to write one of these methods for each array
type.


*/




import java.util.*;
import java.lang.reflect.Array;


public class Test{

    public static void main (String s[]) {

	int mySize = 10;
	int[]  myIntArray;
	double[]  myDoubleArray;
	boolean[] myBooleanArray;


        myIntArray = new int[mySize];
	myDoubleArray = new double[mySize];
	myBooleanArray = new boolean[mySize];


	System.out.println("Lets see what these arrays have in them before we do anything to put anything in them:");
	System.out.println("Here is the function printTheArray() of each one:");
	printTheArray(myIntArray);

	printTheArray(myDoubleArray);
	printTheArray(myBooleanArray);


	//NOW set some values, then print again.

	myIntArray[3] = 5;
	myIntArray[7] = 3*myIntArray[3];

	myDoubleArray[7] = 6.666;

	myBooleanArray[4] = true;

	System.out.println("\n After setting a few values, here is the printout:");
	printTheArray(myIntArray);
	printTheArray(myDoubleArray);
	printTheArray(myBooleanArray);

	

	int[] mySmallInt;

	mySmallInt = myIntArray;
	myIntArray = null;

	myIntArray = new int[20];

	for(int i=0; i<mySmallInt.length; i++){
	    myIntArray[i] = mySmallInt[i];
	}


	mySmallInt[9] = 222332;

	myIntArray[15] =999;

	System.out.println("myIntArray: is it resized?");
	printTheArray(myIntArray);


	}
    
    public static void printTheArray (int[] whatever){
	System.out.println ("I was just instructed to spew out an integer array");

	//int n= 10;
	int n= whatever.length;
	System.out.println ("This array says it has " + n + " members.");

	for( int i=0; i < n ; i++){
	    System.out.print(" " +  whatever[i]);
	}
	System.out.println("\nThat was the array");
    }

   public static void printTheArray (double[] whatever){
	System.out.println ("I was just instructed to spew out a double array");

	//int n= 10;
	int n= whatever.length;
	System.out.println ("This array says it has " + n + " members.");

	for( int i=0; i < n ; i++){
	    System.out.print(" " +  whatever[i]);
	}
	System.out.println("\nThat was the array");
    }

    public static void printTheArray (boolean[] whatever){
	System.out.println ("I was just instructed to spew out a boolean array");

	//int n= 10;
	int n= whatever.length;
	System.out.println ("This array says it has " + n + " members.");

	for( int i=0; i < n ; i++){
	    System.out.print(" " +  whatever[i]);
	}
	System.out.println("\nThat was the array");
    }
}
