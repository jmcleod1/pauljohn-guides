//Now create teacher as an instance variable in Student
//I expected this program to take much more memory than
//the one in which the static variable is used.
//I was disappointed and surprised.  Apparently, even
//when a variable is static, there is a copy of it in
//every instance?  I'm still checking

import java.util.Random;

import java.lang.String;

public class Test{

    public static void main (String s[]) {

	int mySize = 100000;
	String[]  myStringArray ;

	myStringArray = new String[mySize];

	Random myGenerator = new Random();
	

	for (int i = 0; i < mySize; i++){
	    int aValue = myGenerator.nextInt();
	    String aString = String.valueOf(aValue);
	    myStringArray[i] = new String(aString);
	}
       
	Student[]  myStudentArray;
		
 	myStudentArray = new Student[mySize];
	
	
	
	//Now create students and give them names out of myStringArray

	for (int i=0; i< mySize; i++){
	    myStudentArray[i] = new Student(myStringArray[i]);
	    myStudentArray[i].setTeacherName("superMan");
	}
	
	//Student.setTeacherName("superMan");

	//String aString = Student.getTeacherName();

	myStudentArray[4].sayHiTo( myStudentArray[1]);


	myStudentArray[4].sayHiTo( myStudentArray[4]);
	

	//Then ask for a random number
	System.out.println("The next random number is " + myGenerator.nextInt());


	//Then ask for a random number between 0 and 10, not including 10

	System.out.println("The next random number in {0,...,9} is " + myGenerator.nextInt(10));


	//Now lets over and over tell these artificial students to greet one another

	for (int i = 0; i < 100; i++) {
	    Student x = myStudentArray[myGenerator.nextInt(10)];
	    Student y = myStudentArray[myGenerator.nextInt(10)];
		
	    x.sayHiTo(y);
	}

	long memTotal = Runtime.getRuntime().totalMemory();

	System.out.println("total vm size:" + memTotal);

	long memFree =  Runtime.getRuntime().freeMemory();

	long memDiff = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	System.out.println("memory usage diff:" + memDiff);

    }

}
