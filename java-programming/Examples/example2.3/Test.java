// There is no substitute for patience and some time looking through
// the jdk docs from Sun. They have a huge number of classes, and if
// you learn what to look for, you can figure out a lot.

// Consider, for instance, the importance of the term "static".  If a
// method is static, it means it is a method that you can have the
// class itself execute for you.  If there is a class Student, a
// static method like "getTeacherName()" could be used to find out the
// teacher's name.

// You also can have an instance method, one that an instance of the
// Student class can execute.

// Note I'm creating students with a constructor that sets their name.
// But before I create the instances, I also tell the class to set the
// teacher name.

// Each static variable can be set directly, as in Student.teacher="bob";
// or it can be set with a static, or class, method, which is called 
// setTeacherName().  However, each student has the
// variable teacher, and all students have that same variable's value.

// Now, just to make the point clear, I am going to create an instance
// method with almost the same name, getTeachersName().  This name
// cannot be exactly the same as the static method.

// Now, why would you ever want a static variable or method?
// The big reason is to synchronize the information available to all
// agents.  In other languages, you save memory by using a class
// variable.  But if you look at example 2.4 and 2.5, you see
// I tried to prove the difference and failed.  I don't konw why.

import java.lang.Object;
import java.lang.String;

public class Test extends Object{


    public static void main (String[] args) {

	Student.setTeacherName("superMan");

	String aString = Student.getTeacherName();

	System.out.println("The Student class said its teacher is named " + aString);
	System.out.println("Isn't it great? I did not even create a student instance yet and I can still setup the Student class and tell it the teacher");

	Student frank = new Student("frank");
	Student katie = new Student("katie");
    
	frank.sayHiTo(katie);


	System.out.println ("Now, frank, what is your teacher's name " + frank.getTeachersName() );
	   
	/*
	  Tell the instance frank to change the
	  teacher's name. I don't think it should work, but in java
	  it does.  The instance can change a class variable.
	*/
	frank.setTeacherName("MudBall");


	System.out.println ("Now, frank, what is your teacher's name " + frank.getTeachersName() );

	System.out.println ("Now, katie, what is your teacher's name " + katie.getTeachersName() );


    }
}
