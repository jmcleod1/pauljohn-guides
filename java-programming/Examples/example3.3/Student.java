/** Student Class.  Is created and given a string for a name.  
* Students are the primary kind of "agent" in this model. 
*/


import java.util.HashSet;  //for the contact list
import java.util.Iterator;
import java.lang.Math;  //use Math.random for random draws in (0,1)

public class Student extends Object {
    /**An instance variable to hold the name of the agent
     */
    String myName;
    
    /**A list holding objects representing other agents.
     * This is created in the model and set in here.
     */
    HashSet contacts;


    /** Create a student and tell it its name
     */
    public Student (String s){
	myName = s;
    }

    /** The student is supposed to send a message to the target Object.
     *  Please note that the target is cast as a Student before it is told
     *  what to do.  You might have other kinds of targets and then you
     *  would need a different cast.
     */
    public void sayHiTo (Object target){
	Student input = (Student) target;
	String message = "hello from " + myName;
	input.receiveMessage(this, message);
    }

    /** The Student object receives a message from "obj" and the
     * content of the message is a String s
     * Note I'm inconsistent here in setting a precise type for
     * obj in this method, whereas I used Object in sayHiTo().
     */    

    public void receiveMessage(Student obj, String s){
	String aName =  obj.getName();
	System.out.println("I, " + myName + ", was greeted by " + aName);
	System.out.println("The message was \"" + s + "\"");
	
	// uncomment this line, see what happens!
	// this.sayHiTo (obj);
    }


    /** Return the student's name string
     */
    public String getName (){
	return myName;
    }


    /** This is a diagnostic method.  When a student is told to sayHi(),
     * it just prints a line to the terminal telling its name.
     * This way, I can check if a student exists.
     */

    public void sayHi(){
	System.out.println("Hello, my name is " + myName);
    }


    public void setContactList(HashSet aList){
	contacts = aList;
    }



    /* randomly choose an agent from the contact list and say hello.
     * Note I'm being cute here to choose not equally likely outcomes.
     */
    public void step (){
	double  aRandNum = Math.random();
	int target;


	if (aRandNum < .4) target = 0;
	else if (aRandNum < .8) target = 1;
	else target = 2;

	System.out.println("target = " + target);
	
	Iterator index = contacts.iterator();
	Student aCandidate = null;
	int offset = 0;

	do{
	    offset++;
	    aCandidate = (Student) index.next();
	} while (offset < target +1);

	this.sayHiTo(aCandidate);  //"this" is optional here
			//but it helps keep it clear.    
 
    }

}
