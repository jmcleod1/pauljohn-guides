/**
 * Model.java
 *
 *
 * Created: Wed Sep 19 09:10:59 2001
 *
 * @author Paul Johnson <a href="mailto:pauljohn@ukans.edu"></a>
 * @version 3.1
 */

import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;  //for static methods like shuffle and copy
import java.util.Random;
import java.util.HashSet;


/** The Model class has methods that build objects and it also tells
 * them to do things.  Please note the use of both Lists and Sets here.
 * For each student, it creates a set of "contacts" that serves an
 * important role in the Student class.  */

public class Model extends Object {
/** The initial number of agents 
*/
  final int N;
/** A linked list container for agents */
  LinkedList agentList;


  public Model (int n){
    N = n;
  }

  /**Create a List and fill it with N student objects.  Then, for each
   * student, create a container for contacts, and randomly choose
   * agents from the agentList to go into the contact list
**/
  public void buildObjects(){

    agentList = new LinkedList();
    Random myGenerator = new Random(77777);

    for (int i = 0; i < N; i++) {
      Student aStudent = new Student("student" + i);
      agentList.addLast(aStudent);
    }

    
    //For each student, I need to create a contact list. I'll use a
    //Set to hold the contacts.  I want to take three agents from the
    //list at random and make them the contacts of a student.  So I
    //have to iterate over the list of students, and for each one
    //select some contacts.  I create a HashSet to hold the
    //contacts. That way I never get duplicates.  I also have to be
    //careful not to put the agent itself in its own contact
    //collection. Maybe there's an easy java way to do it, I don't
    //find it.

    for (Iterator index = agentList.iterator(); index.hasNext(); ){
	Student aStudent= (Student) index.next();
	HashSet contacts = new HashSet();
	
	int nOfContacts = 0;
	Student aCandidate = null;

	//keep looking until we have 3 contacts
	while (nOfContacts < 3){
	    // draw a random student:
	    do {
		int anInt;
		System.out.println(".");
		anInt =  myGenerator.nextInt(N);
		aCandidate = (Student) agentList.get(anInt);
		System.out.println("anInt " + anInt + "sayhi");
		aCandidate.sayHi();
	    } while (aCandidate == aStudent);  
	    // don't add student to own contact list
	    // so keep drawing candidates if you get one is the same as
	    // aStudent

	    
	    // Note: when you add to a set, it returns true if that object
	    // gets added (was not a duplicate).
	    if ( contacts.add(aCandidate) == true ) 
		{
		    nOfContacts++;
		}
	}
		    
	aStudent.setContactList(contacts);
    }
    
  }

  /**Tell the agents to sayHi 
   */

  public void checkAgents(){

    System.out.println("First, here we traverse the list in order");
    
    for (Iterator index = agentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
    }
  }




    /** We want to repeatedly process the list of agents. So we
     * create a for loop to control the number of repetitions, then
     * we step through the list, one agent at a time.  Note that
     * if agents are born and added to the list, we need not change
     * this code, because the whole list will get processed each time
     */ 
    public void runTheSimulation(int n) {

	for (int i = 0; i < n; i++) {
	
	    System.out.println("\n Now we enter the next stage of the simulation. \n  According to our records, this is step number: " + i + " of " + n );

	    for (Iterator index = agentList.iterator(); index.hasNext(); ){
		Student aStudent= (Student) index.next();
		aStudent.step();
	    }
	}
    }

}// Model











