/** None of my students gave me a more interesting story
 * to make code about, so we continue with the stupid story
 * of the students.

 * In this model, we have some big innovations. 

 * Model has a runTheSimulation() method, which will go through the
 * list of agents a given number of times. Each time through it tells
 * each agent to "step", which means "do your business". 

 * Note the buildObjects method in the Model has an additional. It
 * tells gives each agent a list of other agents. But, just for fun
 * it does not give all of the agents in the full list, it gives them
 * a random sample of 3 agents.  

 */


import java.lang.Object;
import java.lang.String;


public class Test extends Object{


    public static void main (String[] args) {

      int nOfAgents = 10;

      Model aModel = new Model(nOfAgents);

      aModel.buildObjects();

      aModel.runTheSimulation(10);
    }

}


