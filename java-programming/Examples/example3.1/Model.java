/**
 * Model.java
 *
 *
 * Created: Wed Sep 19 09:10:59 2001
 *
 * @author Paul Johnson <a href="mailto:pauljohn@ukans.edu"></a>
 * @version 3.1
 */

import java.util.LinkedList;
import java.util.Iterator;


public class Model extends Object {
/** The initial number of agents 
*/
  final int N;
/** A linked list container for agents 
*/
  LinkedList agentList;


  public Model (int n){
    N = n;
  }

  /**Create a List and fill it with N student objects.
  **/

  
  public void buildObjects(){

    agentList = new LinkedList();

    for (int i = 0; i < N; i++) {
      Student aStudent = new Student("student" + i);

      agentList.addLast(aStudent);
    }
  }

  /**Use an iterator to go through the list, one agent at a time. 
<p>
     Please note that there are different ways of using iterators
     to step through lists.
<p>
     The first way I learned was the while() approach shown first.
     The second way is the "for loop" way java experts tend to do it.
   */

  public void checkAgents(){
    
    Iterator index = agentList.iterator();

    // Note vital point that object coming out of list must
    // be cast as a student. This tells java what kind of object
    // it is supposed to be. Otherwise, java only knows it is
    // an Object and it does not know what methods it has.
    while(index.hasNext()) {
      Student aStudent = (Student) index.next();
      aStudent.sayHi();
    }

    //create another iterator, just to show the usage of a for loop

   
    // recall format is:
    //   for (start; while; do_after)
    // So below I have
    // start: create an iterator.  
    // while: there is a next.
    // do_after: nothing!

    // this is more terse, as we create the iterator and the loop in a
    // single line of code.
    System.out.println("Now see the output of the for loop iterator");

    for (Iterator index2 = agentList.iterator(); index2.hasNext(); ){
      Student aStudent= (Student) index2.next();
      aStudent.sayHi();
    }
  }
}// Model




