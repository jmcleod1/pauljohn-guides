/** Student Class.  Is created and given a string for a name.  
* Students are the primary kind of "agent" in this model. 
*/


public class Student extends Object {
/**An instance variable to hold the name of the agent
*/
  String myName;
/** Create a student and tell it its name
*/
  public Student (String s){
    myName = s;
  }

/** The student is supposed to send a message to the target Object.
*  Please note that the target is cast as a Student before it is told
*  what to do.  You might have other kinds of targets and then you
*  would need a different cast.
*/
  public void sayHiTo (Object target){
    Student input = (Student) target;
    String message = "hello from " + myName;
    input.receiveMessage(this, message);
  }

/** The Student object receives a message from "obj" and the
* content of the message is a String s
* Note I'm inconsistent here in setting a precise type for
* obj in this method, whereas I used Object in sayHiTo().
*/    

  public void receiveMessage(Student obj, String s){
    String aName =  obj.getName();
    System.out.println("I, " + myName + ", was greeted by " + aName);
    System.out.println("The message was \"" + s + "\"");
	
    // uncomment this line, see what happens!
    // this.sayHiTo (obj);
  }


/** Return the student's name string
*/
  public String getName (){
    return myName;
  }


/** This is a diagnostic method.  When a student is told to sayHi(),
* it just prints a line to the terminal telling its name.
* This way, I can check if a student exists.
*/

  public void sayHi(){
    System.out.println("Hello, my name is " + myName);
  }
}
