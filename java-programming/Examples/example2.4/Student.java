public class Student extends Object {
    static String teacher;  //look, it's a class variable.
    String myName;  //look, its an instance variable

    public Student (String s){
	myName = s;
    }

    public static void setTeacherName(String s){
	teacher = s;
    }

    public static String getTeacherName() {
	return teacher;
    }


    public void sayHiTo (Object target){
	Student input = (Student) target;
	String message = "hello from " + myName;
	input.receiveMessage(this, message);
    }

    public String getTeachersName() {
	return teacher;
    }



    public void receiveMessage(Student obj, String s){
	String aName =  obj.getName();
	System.out.println("I, " + myName + ", was greeted by " + aName);
	System.out.println("The message was \"" + s + "\"");
	
	// uncomment this line, see what happens!
	// this.sayHiTo (obj);
    }

    public String getName (){
	return myName;
    }
}
