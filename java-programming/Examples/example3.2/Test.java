
import java.lang.Object;
import java.lang.String;

/*Create a Model object and tell it to build some objects
 * and then do the checkAgents() method.
 */

public class Test extends Object{


    public static void main (String[] args) {

      int nOfAgents = 10;

      Model aModel = new Model(nOfAgents);

      aModel.buildObjects();

      aModel.checkAgents();
    }

}


