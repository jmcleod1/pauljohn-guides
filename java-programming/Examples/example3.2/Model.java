/**
 * Model.java
 *
 *
 * Created: Wed Sep 19 09:10:59 2001
 *
 * @author Paul Johnson <a href="mailto:pauljohn@ukans.edu"></a>
 * @version 3.1
 */

import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;  //for static methods like shuffle and copy


public class Model extends Object {
/** The initial number of agents 
*/
  final int N;
/** A linked list container for agents 
*/
  LinkedList agentList;


  public Model (int n){
    N = n;
  }

  /**Create a List and fill it with N student objects.
  **/

  
  public void buildObjects(){

    agentList = new LinkedList();

    for (int i = 0; i < N; i++) {
      Student aStudent = new Student("student" + i);

      agentList.addLast(aStudent);
    }
  }

  /**Tell the agents to sayHi in various ways.  This method shows how
   * to create another LinkedList which is a copy of the original.  It
   * shows how to shuffle it, how to remove an element by its position
   * in a List, How to get a reference to an object from one list and
   * remove that object by name from the other list.  Note the remove
   * method returns a true/false variable, telling whether it found
   * the object you were looking for 
   */

  public void checkAgents(){

    System.out.println("First, here we traverse the list in order");
    
    for (Iterator index = agentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
    }

    
    // here is a new list object, where we will put a copy 
    // of the first.  The LinkedList class has a constructor that 
    // allows for this easily!

    LinkedList copyOfAgentList = new LinkedList(agentList);

    Collections.shuffle(copyOfAgentList);

    System.out.println("\n Now here is traverse of the shuffled copy of the list");
    for (Iterator index = copyOfAgentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
    }


    // Now note I can remove some agents from the copyOfAgentList but
    // the original agentList is unaffected

    copyOfAgentList.remove(2);
    copyOfAgentList.remove(8);
    
    System.out.println("\n Now the copyOfAgentList has " + copyOfAgentList.size() + " elements, but the original agentList still has " + agentList.size() + " elements");

    System.out.println("I'll use the \"indexOf\" method from the LinkedList class to find out from the list where aStudent is in there");

    System.out.println("\n See, here is the original list");
    for (Iterator index = agentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
      System.out.println("I am at this position in the list " + agentList.indexOf(aStudent));
      
    }

    System.out.println("\n But the new list has:");
    for (Iterator index = copyOfAgentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
     System.out.println("I am at this position in the list " + agentList.indexOf(aStudent));
    }


    //Now, for fun, let's do this. Find the 4th agent in agentList, then
    // Tell copyOfAgentList to find that one and remove it! How cruel.

    Student markedStudent = (Student) agentList.get(3);
    boolean test = copyOfAgentList.remove(markedStudent);

    System.out.println ("The boolean test is " + test );
    if (test == true) System.out.println("That means the agent was found in the copy and removed");
    else System.out.println("That means the agent was not found");

    System.out.println("\n \nWhy don't you make an exercise out of editing the Model.java file to print out the list after this agent was removed. Then shuffle it again, see what it has, remove one or two, etc.");


  }
}// Model











