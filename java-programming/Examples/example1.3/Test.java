//This example is just here to show some mathematics.

//The class "BigCalculator" has methods that can answer some math questions.

//Note inside BigCalculator there is an instance variable (IVAR) "magicNumber".
//It is a 'record keeper'.

//Other than that, there's not too much exciting here.


import java.lang.Object;
import java.lang.String;

public class Test extends Object{


    public static void main (String[] args) {
	double d;

	BigCalculator myCalculator = new BigCalculator();
	d = myCalculator.multiplyAndAdd(8,4.343);
	System.out.println("multiplyAndAdd(8,4.343)= " + d);

	d = myCalculator.divideAndAdd(32, 2.12);
	System.out.println("divideAndAdd(32,2.12) = " + d);

	System.out.println("Calculator's magic number is " + myCalculator.getMagicNumber());

	System.out.println("Please note java has the same \"problem\" with division of integers that is found in C");

	System.out.println("For example, 3/7 is " + 3/7);

	System.out.println("But you don't have the same trouble if one of those is a double variable");
	System.out.println("For example, 3/7.0 is " + 3/7.0);

	System.out.println("For example, 3.0/7 is " + 3.0/7);
    }
}
