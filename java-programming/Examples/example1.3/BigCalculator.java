public class BigCalculator extends Object {

    //here is an instance variable.
    double magicNumber;

    public BigCalculator(){ 
	//this constructor does nothing!
    }


    public double multiplyAndAdd(int x, double y){
	magicNumber = x*y + y;
	return magicNumber;
    }

    public double divideAndAdd(int x, double y){
	double z = x/y + y;
	return z;
    }

    public double getMagicNumber(){
	return magicNumber;
    }


}
