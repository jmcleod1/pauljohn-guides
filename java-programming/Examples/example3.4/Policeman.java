import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections; 
import java.util.Random;
import java.util.HashSet;


/**
 * Policeman.java
 *
 *
 * Created: Thu Sep 20 23:33:24 2001
 *
 * @author <a href="mailto:pauljohn@pjdell.pols.ukans.edu">Paul E. Johnson</a>
 * @version
 */

/** When a crime occurs, a Policeman instance is created. Note the
 * constructor gives this "cop" a list of suspects.  The cop then asks
 * around while investigating, and if the fingerprint is recognized by
 * one of the students, then the cop returns that suspect. */

public class Policeman extends Object {
    LinkedList agentList;
    Random myGenerator;
    public Policeman (LinkedList aList){
	agentList = aList;
	myGenerator = new Random(54545);
    }


    /**Oh, heck, there's a crime! and the fingerprint is x. So lets
       ask around.  Check with 4 different Citizens. Ask each if they
       know anybody with the given fingerprint.  If they do, then
       return that student as the prime suspect. Otherwise, ask until
       you have checked with 4 students. */

    public Student investigateCrime(Fingerprint x){
		
	int nOfContacts;
	HashSet contacts = new HashSet();
	int nOfSuspects = agentList.size();

	for(nOfContacts=0; nOfContacts < 4; ){
	    // draw a random student:
	    int anInt;

	    anInt =  myGenerator.nextInt(nOfSuspects);
	    System.out.println("myGenerator output " + anInt);
	    Student aCandidate = (Student) agentList.get(anInt);
	  
	    if (contacts.add(aCandidate) == true) nOfContacts++;  
	    
	    Student aSuspect = aCandidate.checkFingerprint(x);
	    if (aSuspect != null) return aSuspect;
	}

	return null; //caught nobody
    }


}// Policeman
