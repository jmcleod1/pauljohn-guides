/**
 * Fingerprint.java
 *
 *
 * Created: Thu Sep 20 23:18:17 2001
 *
 * @author <a href="mailto:pauljohn@ukans.edu">Paul E. Johnson</a>
 * @version  1
 */

/** Here, a fingerprint is just a random number. But it could be anything,
* including a collection of personal traits owned by the suspect.  The 
* important thing is that it is unique, and so if you have another's
* fingerprint object, you can make sure a randomly drawn other person
* does or does not match that fingerprint */

public class Fingerprint extends Object {
    double secretNumber;
    public Fingerprint (double x){
	secretNumber = x;
    }
    
}// Fingerprint
