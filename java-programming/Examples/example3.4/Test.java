//  /** 

//   * In this model, we have some big innovations. We have to create policeman
//   * and fingerprints and records.  All that detail is handled in the lower
//   * classes, so the Test.java file doesn't have to be changed.
//   */


//  import java.lang.Object;
//  import java.lang.String;

/**
Test is the class that holds the main() program, the one
that must be run if the java program is to work.
You can rename this file anything you want, as long as
you change the first line here to match:
*/

public class Test extends Object{


    public static void main (String[] args) {

      int nOfAgents = 10;

      Model aModel = new Model(nOfAgents);

      aModel.buildObjects();

      aModel.runTheSimulation(10);
    }

}


