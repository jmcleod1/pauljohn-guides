/** Student Class.  Is created and given a string for a name.  
* Students are the primary kind of "agent" in this model. 
*/


import java.util.HashSet;  //for the contact list
import java.util.HashMap;  //for the fingerprints
import java.util.Iterator;
import java.lang.Math;  //use Math.random for random draws in (0,1)

/** The Student class is a basic type of this model.  The Model
 * creates several instances of the student type of "agent".  Each
 * student has a name, it can send messages to other students, receive
 * messages.  The big addition in this model is that the student
 * interacts with others, and when it does, it takes a note of the
 * fingerprint of the other student, and then it puts that other into
 * a map, using the fingerprint as a key.  When asked to check its
 * records on a fingerprint, the agent can then look int he map, see
 * if there is anybody with that fingerprint, and then send back the
 * identity of the other. */


public class Student extends Object {
    /**An instance variable to hold the name of the agent
     */
    String myName;
    
    /**A personal trait*/
    Fingerprint myFingerprint;

    /**A list holding objects representing other agents.
     * This is created in the model and set in here.
     */
    HashSet contacts;
    /** A map storing the identities of other agents, using
       fingerprints as keys. */

    HashMap fpRecords;

    /** Create a student and tell it its name. Create the fingerprint Map */
    public Student (String s){
	myName = s;
	fpRecords = new HashMap(3); //guess there will be 3
	//items. It will grow itself if more are added.
	myFingerprint = new Fingerprint(Math.random());
    }

    /** The student is supposed to send a message to the target Object.
     *  Please note that the target is cast as a Student before it is told
     *  what to do.  You might have other kinds of targets and then you
     *  would need a different cast.
     */
    public void sayHiTo (Object target){
	Student input = (Student) target;
	String message = "hello from " + myName;
	input.receiveMessage(this, message);
    }

    /** The Student object receives a message from "obj" and the
     * content of the message is a String s
     * Note I'm inconsistent here in setting a precise type for
     * obj in this method, whereas I used Object in sayHiTo().
     */    

    public void receiveMessage(Student obj, String s){
	String aName =  obj.getName();
	System.out.println("I, " + myName + ", was greeted by " + aName);
	System.out.println("The message was \"" + s + "\"");
	
	//get that other guy's fingerprints

	Fingerprint aFinger = obj.getFingerprint(); //copy down his fp
	fpRecords.put(aFinger,obj); //put other in map, using his fp as key
    }


    /** Return the student's name string
     */
    public String getName (){
	return myName;
    }


    /**Return the fingerprint object*/
    public Fingerprint getFingerprint(){
	return myFingerprint;
    }

    /** This is a diagnostic method.  When a student is told to sayHi(),
     * it just prints a line to the terminal telling its name.
     * This way, I can check if a student exists.
     */

    public void sayHi(){
	System.out.println("Hello, my name is " + myName);
    }


    public void setContactList(HashSet aList){
	contacts = aList;
    }

    /** Find out if fingerprint of suspect is in records. If so, 
     * send that person's name to the requester.  Otherwise, send
     * null back to them. */

    public Student checkFingerprint(Fingerprint x){
	System.out.println("I'm checking the fingerprint " + x);
	return (Student) fpRecords.get(x);
    }


    /* randomly choose an agent from the contact list and say hello.
     * Note I'm being cute here to choose not equally likely outcomes.
     */
    public void step (){
	double  aRandNum = Math.random();
	int target;


	if (aRandNum < .4) target = 0;
	else if (aRandNum < .8) target = 1;
	else target = 2;

	//	System.out.println("target = " + target);
	
	Iterator index = contacts.iterator();
	Student aCandidate = null;
	int offset = 0;

	do{
	    offset++;
	    aCandidate = (Student) index.next();
	} while (offset < target +1);

	this.sayHiTo(aCandidate);  //"this" is optional here
 

	// System.out.println("fp record " + fpRecords);
    }

}


