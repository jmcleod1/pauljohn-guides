/**
 * Model.java
 *
 *
 * Created: Wed Sep 19 09:10:59 2001
 *
 * @author Paul Johnson <a href="mailto:pauljohn@ukans.edu"></a>
 * @version 3.1
 */

import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;  //for static methods like shuffle and copy
import java.util.Random;
import java.util.HashSet;


/** The Model class has methods that build objects and it also tells
* them to do things.  Please note the use of both Lists and Sets here.
* For each student, it creates a set of "contacts" that serves an
* important role in the Student class.  Note also that when a crime
* occurs, a Policeman is created and told to investigate.
<p>
* One interesting issue is that some variables have to be "promoted" to IVARs in
* order to use them in several different methods.  Now myGenerator is an instance
* variable, for that reason.  */

public class Model extends Object {
    /** The initial number of agents 
     */
    final int N;
    /** A linked list container for agents */
    LinkedList agentList;

    /**A random number generator that is used in several methods. Hence, it must
       be an IVAR in order to be available throughout the class */
    Random myGenerator;
    


  public Model (int n){
    N = n;
    //if you want the same random number every run, do this:
    //myGenerator = new Random(77777);
    //if you want a different "suspect" every run, do this:
	myGenerator = new Random();
  }

  /**Create a List and fill it with N student objects.  Then, for each
   * student, create a container for contacts, and randomly choose
   * agents from the agentList to go into the contact list
**/
  public void buildObjects(){

    agentList = new LinkedList();
  

    for (int i = 0; i < N; i++) {
      Student aStudent = new Student("student" + i);
      agentList.addLast(aStudent);
    }

    
    //For each student, I need to create a contact list. I'll use a
    //Set to hold the contacts.  I want to take three agents from the
    //list at random and make them the contacts of a student.  So I
    //have to iterate over the list of students, and for each one
    //select some contacts.  I create a HashSet to hold the
    //contacts. That way I never get duplicates.  I also have to be
    //careful not to put the agent itself in its own contact
    //collection. Maybe there's an easy java way to do it, I don't
    //find it.

    for (Iterator index = agentList.iterator(); index.hasNext(); ){
	Student aStudent= (Student) index.next();
	HashSet contacts = new HashSet();
	
	int nOfContacts = 0;
	Student aCandidate = null;

	//keep looking until we have 3 contacts
	while (nOfContacts < 3){
	    // draw a random student:
	    do {
		int anInt;
		System.out.println(".");
		anInt =  myGenerator.nextInt(N);
		aCandidate = (Student) agentList.get(anInt);
		System.out.println("anInt " + anInt + "sayhi");
		aCandidate.sayHi();
	    } while (aCandidate == aStudent);  
	    // don't add student to own contact list
	    // so keep drawing candidates if you get one is the same as
	    // aStudent

	    
	    // Note: when you add to a set, it returns true if that object
	    // gets added (was not a duplicate).
	    if ( contacts.add(aCandidate) == true ) 
		{
		    nOfContacts++;
		}
	}
		    
	aStudent.setContactList(contacts);
    }
    
  }

  /**Tell the agents to sayHi 
   */

  public void checkAgents(){

    System.out.println("First, here we traverse the list in order");
    
    for (Iterator index = agentList.iterator(); index.hasNext(); ){
      Student aStudent= (Student) index.next();
      aStudent.sayHi();
    }
  }




    /** We want to repeatedly process the list of agents. So we
     * create a for loop to control the number of repetitions, then
     * we step through the list, one agent at a time.  Note that
     * if agents are born and added to the list, we need not change
     * this code, because the whole list will get processed each time
     */ 
    public void runTheSimulation(int n) {

	Fingerprint criminalPrint;

	for (int i = 0; i < n; i++) {
	
	    System.out.println("\n Now we enter the next stage of the simulation. \n  According to our records, this is step number: " + i + " of " + n );

	    for (Iterator index = agentList.iterator(); index.hasNext(); ){
		Student aStudent= (Student) index.next();
		aStudent.step();
	    }
	}

	System.out.println( "Students finished interacting. Now turn the cop loose");
	
	criminalPrint = ((Student) agentList.get( myGenerator.nextInt(N) )).getFingerprint();
	
	Policeman theCop = new Policeman(agentList);

	Student primeSuspect = theCop.investigateCrime(criminalPrint);
	

	if (primeSuspect == null){
	    System.out.println("Policeman says he can't find a suspect");
	}
	else{
	     System.out.println("Policeman found a suspect");
	     System.out.println("The suspect's name is " + primeSuspect.getName());
	}	    

    }

}// Model











