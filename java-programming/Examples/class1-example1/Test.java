//I want to show you the idea of polymorphism, sometimes called
// "overloading."  The student class has several methods named
// "doThat" and notice each one takes different arguments.  

// When we use that method "doThat()" we change the arguments,
// and the objects figure out which method to use.

import java.lang.Object;
import java.lang.String;

public class Test extends Object{


    public static void main (String[] args) {
	Student frank = new Student("frank");
	Student katie = new Student("katie");
	int x = 7;
	double y = 1.10;
	double z = 2.3232;

	frank.doThat(x);
	frank.doThat(y);
	frank.doThat(y,z);
	frank.doThat(y,z,x);


	x = 912;
	y = 1234.1234;
	z = 444.44;

	katie.doThat(x);
	katie.doThat(y);
	katie.doThat(y,z);
	katie.doThat(y,z,x);
   
    }
}
