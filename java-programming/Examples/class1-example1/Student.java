public class Student extends Object {

    String myName;

    public Student (String s){
	myName = s;
    }

    public void doThat (double d){
	System.out.println(myName + " can doThat do a double " + d);
    }

    public void doThat (int i){
	System.out.println(myName + " can doThat to an integer " + i);
    }

    public void doThat (double d1, double d2){
	System.out.println(myName + " can doThat to two double arguments, " + d1 + " and " + d2);
    }
    
      public void doThat (double d1, double d2, int i){
	System.out.println(myName + " can doThat to two double arguments and an integer, " + d1 + " and " + d2 + " and " + i);
    }
}
