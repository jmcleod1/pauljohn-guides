import java.util.Random;
import java.lang.Math;
import java.awt.Color;


public class HSETest {

  static void main (String []args) {

    int N=10;
    double[][][] positionMatrix = new double[N][2][2];
    double [][] distMatrix = new double[N][N];
    Random randGenerator = new Random();	
	
    SwarmRaster   aRaster;

    aRaster= new SwarmRaster("PJ's Space",100,100,4);


    for (int i = 0; i < N; i++) {
      
	    positionMatrix[i][0][1] = 20 + 10*randGenerator.nextGaussian();
	    positionMatrix[i][1][0] = 20 + 10*randGenerator.nextGaussian();
	    
	    aRaster.drawPointX$Y$Color ((int)positionMatrix[i][0][1],(int)positionMatrix[i][1][0],Color.red.getRGB());
  
      }


    for (int i = 0; i < N; i++){
	for (int j = i+1; j < N; j++){
	    double dist = Math.sqrt((positionMatrix[i][0][1]-positionMatrix[j][0][1])*
			(positionMatrix[i][0][1]-positionMatrix[j][0][1])*
			+  (positionMatrix[i][1][0]-positionMatrix[j][1][0])*
			(positionMatrix[i][1][0]-positionMatrix[j][1][0]));
	    distMatrix[i][j] = dist;
	    distMatrix[j][i] = dist;
	
      }
    }
    for (int i = 0; i < N; i++){
      for (int j = 0; j < N; j++){
	System.out.print ( distMatrix[i][j] + " " );
      }
      System.out.println( "");
    }


    {
      double ent;
      ent = HierSocialEntropy.calculateHSE(N,distMatrix,Math.sqrt(20000)) ;
      System.out.println("\n \n HS Entropy is " + ent);
    }


      
    
  }
}
