/*
Colt is a free java library you can download and install. I unpackaged it 
in a directory. To use it, you have to add it to the classpath.

It has good documents.  There are both static methods and instance
methods, as I try to show below.  The objective is to draw random
normal observations.

In order to use a package like this, one must be careful to set the
CLASSPATH.  In a shell, I do 

export CLASSPATH=/usr/local/java_packages/colt/colt.jar:$CLASSPATH

Or in the Emacs JDE I set some project options. That seems baffling sometimes.

*/




import java.util.*;
import java.lang.reflect.Array;


import cern.jet.random.Normal;
import cern.jet.random.engine.RandomEngine;
import cern.jet.random.engine.MersenneTwister;


public class Test{

    public static void main (String s[]) {

	

	//just need a quick "no fuss" random number?
	double n1 = Normal.staticNextDouble(33,22);

	System.out.println("A staticDouble was " + n1);


//  	Now, it is a little slower to use the static method if you are
//  	drawing lots of numbers. So we can create a new Normal object

	RandomEngine aGen = new MersenneTwister(324234);

	Normal myNormal = new Normal( 33, 22, aGen);

	double n2 = myNormal.nextDouble();
	
	System.out.println("from the generator object, we get:" + n2);


	//So, in case you ever want to draw a lot of random numbers,
	//the way to go is to create a distribution object that is
	// based on the generator you like.

	for (int i = 0; i < 1000; i++){
	    System.out.println("Here's the " + i + "'th draw from N(33,22):" + myNormal.nextDouble());
	}

    }
}
